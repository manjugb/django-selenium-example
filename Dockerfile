FROM python:3.6.2
ENV PYTHONUNBUFFERED 1

WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt
ADD . /code

VOLUME /code

CMD ["python3", "manage.py", "makemigrations", "--noinput"]
CMD ["python3", "manage.py", "migrate", "--noinput"]
CMD ["python3", "manage.py", "test", "--noinput"]
