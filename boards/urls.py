from django.urls import path
from .import views

# /boards
# /boards/1/detail
# /boards/new
urlpatterns = [
    path('',views.index,name='home'),
    path('new',views.new)
]