from django.contrib.auth.models import User
from django.db.models import Count
from django.http import HttpResponse
from django.shortcuts import render

from boards.models import Board, Topic, Post


def index(request):
    boards = Board.objects.all()
    post_count = Post.objects.select_related('posts', 'message').count()
    topic_count = Topic.objects.all().count()
    return render(request, 'index.html', {'boards': boards,'blog_post': post_count,
                                          'topic_count': topic_count })


def new(request):
    return HttpResponse('New Products')
